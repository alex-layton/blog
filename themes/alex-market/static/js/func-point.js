$(document).ready(function() {
    $('.article-entry p img').each(function(i, element) {
        $(this).on('click', function() {
            var src = $(this).attr('src');
            window.open(src);
        });
    });

    $('.article-entry :header').each(function() {
        var id = $(this).attr('id');
        $(this).wrap('<a class="article-entry-header-wrap" href="#'+ id +'"></a>');
    });

    setTimeout(() => {
        $('.article-entry p img').each(function(index, element) {
            var width = element.width;
            if (document.body.clientWidth < width) {
                $(this).css({
                    width: '100%',
                    height: 'auto'
                })
            }
        });
    }, 300);
});