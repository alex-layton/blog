---
title: "构建栈结构函数"
date: 2018-12-02T22:10:18+08:00
draft: false
categories: ['php']
---

{{<highlight php>}}
<?php
$stack = function ($passable) use ($stack, $pipe) {
    $parameters = array_merge([$passable, $stack], $parameters);
    return $pipe->handle($parameters);
};
{{</highlight>}}
