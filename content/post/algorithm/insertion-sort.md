---
title: "插入排序 | Insertion Sort"
date: 2018-12-08T23:06:02+08:00
draft: false
categories: ['算法', '排序']
languages: ['cpp']
description: '插入排序的工作方式类似排序一副扑克牌，为了找到一张牌的正确位置，每次抓到新牌时，我们从右向左对每一张牌进行比较，拿在手中的牌总是排序好的。对于少量元素的排序，插入排序是一个稳定有效的算法。'
---

#### <font color="#708090">算法介绍:</font>
![插入排序 | Insertion Sort](/blog/images/insertion-sort.png "插入排序 | Insertion Sort")  
插入排序的工作方式类似排序一副扑克牌，为了找到一张牌的正确位置，每次抓到新牌时，我们从右向左对每一张牌进行比较，拿在手中的牌总是排序好的。对于少量元素的排序，插入排序是一个稳定有效的算法。

#### <font color="#708090">算法原理:</font>
在每次迭代过程中从输入序列中取出一个元素插入到一个有序序列中，形成新的有序序列。重复该过程，直到序列中所有元素都被取出。

#### <font color="#708090">算法实现（C 语言）:</font>
{{<highlight c>}}
void insertion_sort(int * array)
{
    for (int j = 1; j < sizeof(array); j++)
    {
        int key = array[j];
        int i = j - 1;
        while (i >= 0 && array[i] > key)
        {
            array[i + 1] = array[i];
            i--;
        }
        array[i + 1] = key;
    }
}
{{</highlight>}}

#### <font color="#708090">代码解析:</font>
从左到右，将下标为 `j` 的元素 `key` 与左边的每一项作比较，如果左边元素的值大于 `key`，则将 `key` 向左移动一位，依次循环。
比较是从第 0 位开始的，所以不存在左边比右边的值大的情况。

#### <font color="#708090">相关数值:</font>
| 插入排序  |      数值      |
|------------|:-----------:|
| 时间复杂度 |  $ O(n^2) $ |
| 空间复杂度 |  $ O(1) $ |
| 稳定性        | 稳定      |
| 复杂性        | 简单      |
