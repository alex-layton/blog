---
title: "持续集成 (Continuous Integration)"
date: 2018-12-02T21:24:27+08:00
draft: false
categories: ['运维', '持续集成']
---

### 什么是 Continuous Integration？
{{<highlight php>}}
<?php
$ci = new ContinuousIntegration();
echo 'this is a test';
$ci->start();
{{</highlight>}}
