---
title: "一个 Docker 环境部署的完整过程"
date: 2018-12-10T21:56:47+08:00
draft: false
categories: ['运维', 'docker']
languages: ['yaml', 'bash', 'dockerfile']
description: 记录一个我个人正在开发的小项目的环境搭建过程，完全使用 docker 部署。这是一个最简化的操作流程，暂时没有任何负载优化，在这里把思路串起来，留给后人当个参考吧。
---

## 前言
记录一个我个人正在开发的小项目的环境搭建过程，使用 docker 部署。这是一个最简化的操作流程，暂时没有任何负载优化，在这里把思路串起来，留着当作参考吧。  

#### <font color="#708090">项目中所需要的组件：</font>
- nginx
- php
- mysql-5.7
- redis
- phpmyadmin

## 具体实施：

#### <font color="#4682B4">从官方仓库选择镜像：</font>
docker hub 上的镜像像星星一样多，我们一定要从官方仓库中选择镜像！  
如何区别官方库和三方库呢？所有认证过的官方库都会被标记为 `OFFICIAL`， 而三方库则是 `PUBLIC | AUTOMATED BUILD`。   
如果没有特殊需求，应该选择 alpine 版本的镜像，这种镜像非常小巧，nginx-1.15.2-alpine 配置完成后，大小只有 18.6MB。  
我这里选择的版本分别为：   
{{<highlight zsh>}}
nginx:1.15.2-alpine
php:7.2.8-fpm-alpine3.7
mysql:5.7.23
phpmyadmin/phpmyadmin:latest
redis:4.0.11-alpine
{{</highlight>}}
#### <font color="#4682B4">准备工作：</font>
我的代码和项目部署文件都在同一个目录下，后端是 php，位于 `cgi` 目录，前端是 angularjs，位于 `gui` 目录  
首先要做的就是，在项目根目录下建立 docker-compose.yml 并用你喜欢的编辑器打开它
{{<highlight bash>}}
touch docker-compose.yml
{{</highlight>}}
我的计划是，在外部建立一个 config 目录来放置所有组件的配置文件，logs 目录存放各组件的日志文件，store 目录存放组件生成的持久化数据。在 `docker-compose.yml` 的同级目录建立：
{{<highlight bash>}}
mkdir config
mkdir logs
mkdir store
{{</highlight>}}
部署的过程中，我们会创建自己的 docker 镜像，所以我们需要使用 docker hub 存放新的镜像文件。docker hub 网址是 https://hub.docker.com ，如果你还没有账号，赶快注册一个吧 :)   
在本地登录 docker hub：
{{<highlight bash>}}
docker login
{{</highlight>}}

准备完毕，打开 `docker-compose.yml` 开始部署

#### <font color="#4682B4">部署 nginx：</font>
nginx 和 php 都需要观测到项目文件，所以，需要分别挂载整个项目到 nginx 和 php 的虚拟环境中去
{{<highlight yaml>}}
version: "3"
services:
  nginx:
    image: nginx:1.15.2-alpine
    container_name: nginx
    restart: always
    volumes:
      - .:/app
      - ./config/nginx.conf:/etc/nginx/conf.d/default.conf
    working_dir: /app
    networks:
      - virtual
    ports:
      - "80:80"
...

networks:
  virtual:
{{</highlight>}}
在新环境中，项目位于 `/app` 目录下，
nginx 的配置文件为 `config/nginx.conf`（这篇文章将略过所有组件的配置文件细节）。

#### <font color="#4682B4">部署 php：</font>
php 的环境比较复杂，需要创建一个自定义镜像，来安装需要的 php extention。新建并编写 `Dockerfile`：
{{<highlight dockerfile>}}
FROM php:7.2.8-fpm-alpine3.7

RUN sed -i 's/dl-cdn.alpinelinux.org/mirrors.ustc.edu.cn/g' /etc/apk/repositories \
  && apk add --no-cache freetype libpng libjpeg-turbo freetype-dev libpng-dev libjpeg-turbo-dev git \
  && git clone https://github.com/phpredis/phpredis.git /usr/src/php/ext/redis \
  && docker-php-ext-configure gd \
     --with-gd --with-freetype-dir=/usr/include/ --with-png-dir=/usr/include/ \
     --with-jpeg-dir=/usr/include/ && NPROC=$(grep -c ^processor /proc/cpuinfo 2>/dev/null || 1) \
  && docker-php-ext-install pdo pdo_mysql mbstring redis -j${NPROC} gd opcache \
  && apk del --no-cache freetype-dev libpng-dev libjpeg-turbo-dev \
  && curl -L https://getcomposer.org/installer -o ./composer-setup.php \
  && php composer-setup.php \
  && mv composer.phar /usr/local/bin/composer

WORKDIR /app/cgi
{{</highlight>}}
注意，这里我把 apk 的源从官方替换为清华大学的，因为官方的实在太慢了...   
并且，我们需要从 github 上拉取 phpredis 的源码到 `/usr/src/php/ext/` 目录下，更名为 `redis`，因为 `docker-php-ext-install` 无法找到 phpredis 的资源，只好自己动手了。  

将配置好的 `Dockerfile` 打包成本地镜像，命名为 `ballad-bucket_php`，切换到 `Dockerfile`所在的目录，并运行：
{{<highlight bash>}}
docker build -t ballad-bucket_php ./
docker image ls
{{</highlight>}}
我们看到，在本地已经创建了一个新的镜像文件 `ballad-bucket_php` 。但它还无法在非本地环境中使用，所以我们需要将这个新镜像打上标签（tag）并提交到 docker hub 的 public 仓库中去。
{{<highlight bash>}}
docker tag ballad-bucket_php blldxt/ballad_php:cgi
docker push blldxt/ballad_php:cgi
{{</highlight>}}

现在，我们可以在任何地方使用这个 `blldxt/ballad_php:cgi` 镜像了！

配置 `php` php 配置文件为 `config/php.ini`
{{<highlight yaml>}}
version: "3"
services:
    ...
    php_cgi:
    image: blldxt/ballad_php:cgi
    container_name: php_cgi
    restart: always
    ports:
      - "9000:9000"
    volumes:
      - .:/app
      - ./config/php.ini:/usr/local/etc/php/conf.d/php.ini
    working_dir: /app
    networks:
      - virtual
    ...
networks:
  virtual:
{{</highlight>}}
同 nginx 一样，我们将整个项目挂载到 php 环境中的 `/app` 目录下，以便 fpm 解析。

#### <font color="#4682B4">部署 mysql：</font>
配置 `mysql` 服务，mysql 配置文件为 `config/my.cnf`，数据目录为 `store/mysqldata`。   
{{<highlight yaml>}}
version: "3"
services:
    ...
    db:
    image: mysql:5.7.23
    container_name: mysql
    environment:
      MYSQL_ROOT_PASSWORD: xxxxxx
      # PMA_HOST: mysql
    command: --default-authentication-plugin=mysql_native_password --explicit_defaults_for_timestamp
    restart: always
    volumes:
      - ./config/my.cnf:/etc/mysql/mysql.conf.d/mysqld.cnf
      - ./store/mysqldata:/var/lib/mysql
    ports:
      - "3306:3306"
    networks:
      - virtual
    ...
networks:
  virtual:
{{</highlight>}}

#### <font color="#4682B4">部署 redis：</font>
配置 `redis` redis 的持久化数据目录为 `store/redisdata`。   

{{<highlight yaml>}}
version: "3"
services:
    ...
    redis:
    image: redis:4.0.11-alpine
    container_name: redis
    restart: always
    volumes:
      # - ./config/redis.conf:/etc/redis.conf
      - ./store/redisdata:/data
    environment:
      - ALLOW_EMPTY_PASSWORD=yes
      - REDIS_EXTRA_FLAGS=--maxmemory 100mb
    ports:
      - "6379:6379"
    command: ["redis-server", "--appendonly", "yes"]
    networks:
      - virtual
    ...
networks:
  virtual:
{{</highlight>}}

#### <font color="#4682B4">部署 phpmyadmin：</font>
{{<highlight yaml>}}
version: "3"
services:
    ...
    phpmyadmin:
    image: phpmyadmin/phpmyadmin:latest
    container_name: phpmyadmin
    restart: always
    environment:
     - PMA_ARBITRARY=1
    ports:
      - "8080:80"
    networks:
      - virtual
    ...
networks:
  virtual:
{{</highlight>}}

#### <font color="#4682B4">启动 docker stack：</font>
##### 首先需要了解几个概念：
- docker-compose.yml 中配置的组件叫做 service, docker-compose.yml 叫做 stack file
- 多个 service 组成集群，docker 中的术语叫 “swarm”，是一种分布式环境
- service 集合运行在 `stack` 上，而启动 stack 需要 swarm 模式的支持

开启 swarm 模式：
{{<highlight bash>}}
docker swarm init
{{</highlight>}}
通过 stack file 启动集群，并将当前的 stack 命名为 ballad：
{{<highlight bash>}}
docker stack deploy -c docker-compose.yml ballad
{{</highlight>}}
 现在，我们的环境已经配置完毕，并且运行成功了。可以通过三种渠道访问项目：   
 1） 本地的回环 ip 地址 127.0.0.1   
 1） docker 桥接网络地址   
 2） 局域网或公网 ip

## 相关页面：
Docker 官方文档 https://docs.docker.com/   
Docker Hub https://hub.docker.com   
（docker 注册页使用了 google 点击验证，需要翻墙才行 :(）