---
title: "Redis 持久化"
date: 2018-12-19T23:20:11+08:00
draft: false
categories: ['运维', 'redis']
description: Redis 的两种持久化方式的区别，工作原理和特点。
languages: ['bash', 'ini']
references: [
    {title: 'redis.io —— Redis Persistence', 'uri': 'https://redis.io/topics/persistence#redis-persistence'},
    {title: '云栖社区 —— AOF', 'uri': 'https://yq.aliyun.com/ziliao/435586'}
]
---

redis 是内存数据库，一旦退出进程数据就会丢失，如果想要持久化数据需要启用 redis 持久化功能。redis 提供了两种持久化方案，分别是 RDB 和 AOF 模式。这两种方式各有利弊，下面将分析两种方案的细节和应用场景。

### <li>开启与关闭持久化功能</li>
RDB 模式是默认开启的，配置文件中的选项为
{{<highlight bash>}}
save 900 1
save 300 10
save 60 10000
{{</highlight>}}
表示每 900 秒有1 次写操作/每 300 秒有 10 次写操作/每 60 秒有 10000 次写操作时执行一次 RDB 持久化，创建 rdb 快照。   
想要禁用 RDB 持久化，只要在配置文件中加入
{{<highlight bash>}}
save ""
{{</highlight>}}

AOF 模式默认关闭，想要开启 AOF 模式，需要更改配置项 appendonly 为 yes
{{<highlight bash>}}
appendonly yes
{{</highlight>}}
想要禁用 AOF 模式，则需要将 appendonly 改为 no

### <li>工作原理</li>
#### 1) RDB 模式：
RDB 的持久化方式被称为快照(Snapshotting)。
Snapshotting 的储存文件默认为 dump.rdb，这是一个二进制文件，如果你希望自定义存储文件的名字，可以更改配置选项 dbfilename
{{<highlight bash>}}
dbfilename dump.rdb
{{</highlight>}}
执行 RDB 持久化与调用 BGSAVE 命令的动作相同。开启 RDB 模式后，一旦达到保存数据的条件， redis 将会 fork 一个进程，并创建一个临时的 rdb 文件，当子进程写入完毕后，redis 会将原来的 rdb 文件原子性的替换为新的 rdb 文件。这种工作方式，让 redis 可以从写时复制机制中获益。

#### 2) AOF 模式：
AOF（Append-only file）模式通过 RESP 协议，将每一条命令追加到 AOF 文件中去。如果你希望自定义存储文件的名字，可以更改配置选项 
{{<highlight bash>}}
appendfilename "appendonly.aof"
{{</highlight>}}
你可以选择 3 中同步频率：always/everysec/no
{{<highlight bash>}}
# appendfsync always
appendfsync everysec
# appendfsync no
{{</highlight>}}
always 是每调用一次写入命令就同步一次，everysec 是每秒同步一次，no 是从不同步。默认情况下，也是官方推荐的方式 是 everysec，兼顾了性能与数据的一致性。   
由于写入命令不断累加，AOF 文件会变得越来越大，这时，redis 需要对 AOF 日志做 rewrite 操作，这个操作会将命令集合进行压缩，比如，如果你对一个计数器调用了 100 次 INCR ，将会保存为 SET 100。   
在 Redis 2.2 版本之前，rewrite操作需要手动执行 BGREWRITEAOF 命令，而 2.2 版本之后，redis 会自动执行这个命令，成为了 redis 的一个特性。   
**有 2 个配置选项用来调节自动重写频率**
{{<highlight ini>}}
#  当 Aof log 增长超过指定比例时，重写 log file，设置为0表示不自动重写Aof 日志，重写是为了使 aof 体积保持最小，而确保保存最完整的数据。
auto-aof-rewrite-percentage 100
# 触发aof rewrite的最小文件尺寸
auto-aof-rewrite-min-size 64mb
{{</highlight>}}

#### 3) 与持久化相关的三个命令：
1）SAVE: 执行持久化操作，并阻塞 Redis 主进程。   
2）BGSAVE: 在子进程中执行持久化操作，不会阻塞 Redis 主进程。   
3）BGREWRITEAOF: 执行 AOF 文件重写操作，不会阻塞 Redis 主进程。   
为了性能和原子性考虑， BGSAVE 执行时， SAVE 命令不能执行。BGSAVE 和 BGREWRITEAOF 也不能同时执行。

当 redis 启动时，会根据持久化配置读取 rdb 或 aof 文件，将数据重新载入到内存中。   
当 redis 退出时，会执行一次持久化操作，但如果用 kill -9 中断，则不会执行持久化操作。

### <li>两种模式的特点</li>
#### 1) RDB 模式：
RDB 模式的优点是：rdb 文件是一个二进制的，非常紧凑的文件。节省空间，并且便于传输。rdb 文件的加载和恢复速度比 aof 快一些，总体性能也更好。

RDB 模式的缺点是：rdb 每次都需要 fork 进程，保存完整的数据集，所以为了性能考虑，不能过于频繁的执行持久化操作，当我们使用 RDB 模式时，通常会每隔 5 分钟或更久做一次持久化操作。所以，当出现灾难时，未作保存的一部分数据便会丢失。
#### 2) AOF 模式：
AOF 模式的优点是：它的“耐久度”比 RDB 要高，这得益于 aof 文件的 fync 操作由线程处理，追加操作性能也比较高，你可以更频繁的进行持久化操作了，使用默认的 everysec 策略，你最多会丢失一秒的数据。aof 日志的格式是人类可读的，你可以清晰的看到每次同步做了哪些操作，并修改它。这在很多情况下很有用，可以避免在恢复时重复同样的错误。AOF 模式的写入操作是原子性的，重写操作在另一个进程里执行，所以，无论任何时候 你都可以复制和提取 aof 文件做备份，发生停机时，原有的 aof 文件也不会丢失。

AOF 模式的缺点是：在某些场景下，aof 文件的体积会变得非常大，redis-2.4 引入的日志自动重写功能相对弥补了这一缺陷，但重写后的 aof 文件的体积也通常大于 rdb 文件。AOF 的总体性能低于 RDB，但默认的每秒 sync 也是足够快的了。日志重写功能会 fork 一个进程，并扫描整个数据集，所以存在和 RDB 模式相似的性能问题，但重写频率并不影响数据的耐久度，我们可以调节配置，在空间和性能上做一个权衡。

### <li>修复损坏的 AOF 日志</li>
服务器可能在程序正在对 AOF 文件进行写入时停机， 如果停机造成了 AOF 文件出错（corrupt）， 那么 Redis 在重启时会拒绝载入这个 AOF 文件。这时可以使用 redis 自带的 redis-check-aof 程序修复 aof 文件。
{{<highlight bash>}}
redis-check-aof –fix
{{</highlight>}}

### <li>容灾备份</li>
容灾备份就是将数据备份到不同的外部数据中心，这样做可以在 Redis 发生灾难时，让数据处于安全状态。

每小时将持久化日志或快照加密上传到第三方对象存储服务器中。这些服务可能是 Amazon S3，七牛云存储等。加密可以使用对称加密方式（如 gpg）。