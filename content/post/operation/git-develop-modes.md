---
title: "基于 git 的 Master/Develop 工作流程"
date: 2018-12-16T17:15:12+08:00
draft: false
categories: ['运维', 'git']
languages: ['bash']
description: 这是一种 git 的分布式工作流程（Distributed Workflows），这种简单高效的工作流程已经在许多项目中得到证实。适用于大部分的中小型项目。
---

## master/develop 模式
这是一种 git 的分布式工作流程（Distributed Workflows），这种简单高效的工作流程已经在许多项目中得到证实。   
开始之前，我假设你已经在 gitlab 上创建了一个空项目 gittest，并将项目 clone 到本地。
{{<highlight bash>}}
git clone https://gitlab.com/your-name/gittest.git
{{</highlight>}}

#### <font color="#708090">1）创建 Develop 分支</font>
首先创建一个基于 master 的 develop 分支
{{<highlight bash>}}
git checkout -b develop master
git push origin develop
{{</highlight>}}
develop 用来生成项目的最新版本 (nightly)，develop 分支是常设分支，不应该被删除。

#### <font color="#708090">2）创建 Feature 分支</font>
现在，我们想添加一个 ·Hello World· 功能，则需要基于 develop 创建一个 Feature 分支：
{{<highlight bash>}}
git checkout -b feature_hello_world develop
{{</highlight>}}
在这个分支上添加文件 hello-world.txt，并上传至远程仓库
{{<highlight bash>}}
touch hello-world.txt
git add hello-world.txt
git commit -m "add a new file"
git push origin feature_hello_world
{{</highlight>}}

#### <font color="#708090">3）提交 Merge Request</font>
上传 feature branch 后，节点开发者向 develop 发起 Merge Request。在 gitlab 的 gittest 项目主页面点击 Create merge request，并将 Target branch 变更为 develop。  
一般来说，比较重要的信息是 Description 和 Assignee，填充 Merge Request 表单，并提交 Merge Request。
![Finish The Merger Request](/blog/images/finish-merger-request.png "Target Branch")  

#### <font color="#708090">4）检查并合并分支</font>
开发者提交 Merge Request 时，将 Merge Request 分配给检视人员（Assignee），检视人在本地拉取 feature branch，运行测试，确认无误后，将 feature branch 合并到 develop branch，之后删除 feature branch。   
这里我们拉取 feature_hello_world 到本地
{{<highlight bash>}}
git fetch origin feature_hello_world
git checkout feature_hello_world
{{</highlight>}}
确认 hello-world.txt 已经创建后，我们切换回 develop，合并并删除 feature_hello_world
{{<highlight bash>}}
git checkout develop
git merge feature_hello_world
git push origin develop
git branch -d feature_hello_world
git push origin --delete feature_hello_world
{{</highlight>}}

#### 在 gitlab 上进行操作：
上述操作我们也可以在 gitlab 上进行。检视人员进入 Merge Request 页面，点击 merge 按钮即可，并且支持同时删除 Feature 分支。

#### <font color="#708090">5）创建预发布分支</font>
当想要发布一个版本时，一般需要创建一个预发布分支（release），release 分支基于 develop，开发者在这里做发布前夕的准备工作，包括测试、解决小型 bug 等。   
在预发布分支中，一般不应该存在业务逻辑的变更。较小的变更（如 改字）可以直接提交并 merge 到 release 分支中。
{{<highlight bash>}}
git checkout -b release-1.0.0 develop
git push origin release-1.0.0
{{</highlight>}}

#### <font color="#708090">6）版本发布</font>
最终版本的发布需要将 release 分支合并到 master 和 develop 中，一般情况下，还需要打一个版本号标签，用于标记发布的版本，最后删除预发布分支。
{{<highlight bash>}}
git checkout master
git merge release-1.0.0
git tag -a 1.0.0
git push origin 1.0.0
git push origin master

git checkout develop
git merge release-1.0.0
git push origin develop

git branch -d release-1.0.0
git push origin --delete release-1.0.0
{{</highlight>}}

#### <font color="#708090">7）修复错误</font>
如果发布后的版本出现 bug，需要基于 master 创建 fixbug 分支，进行修复，然后 merge 到 master 和 develop 中，并更新小版本号。
{{<highlight bash>}}
git checkout -b fixbug-1.0.1 master
git push origin fixbug-1.0.1

git checkout master
git merge fixbug-1.0.1
git tag -a 1.0.1
git push origin 1.0.1
git push origin master

git checkout develop
git merge fixbug-1.0.1
git push origin develop

git branch -d fixbug-1.0.1
git push origin --delete fixbug-1.0.1
{{</highlight>}}
一般情况下，项目的 bug 修复只需要更新小版本号，比如，在上面的例子中将版本 1.0.0 变更为 1.0.1 。   
只有重大功能变更时，才更新大版本号。
